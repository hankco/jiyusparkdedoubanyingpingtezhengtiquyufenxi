#encoding=utf-8
# 处理python编码格式
import csv
import sys
reload(sys)
sys.setdefaultencoding('utf8')
# ------------------------

csv_reader = csv.reader(open('./star-comment.csv'))
txt_writer = open('./star-comment.txt', 'w')
for row in csv_reader:
    txt_writer.write(row[0]+'\t'+row[1]+'\n')
txt_writer.close()